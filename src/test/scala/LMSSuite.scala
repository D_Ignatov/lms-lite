package test

import org.scalatest.FunSuite
import scala.virtualization.lms.LoopsPackage
import scala.virtualization.lms.internal.Exp

trait LMSSuite extends FunSuite {
  private val verboseTests_? = System.getProperty("test.verbose", "false").toBoolean

  def debug(message: String) {
    if (verboseTests_?) info(message)
  }

  def emitSourceScala[T : Manifest, R : Manifest](prog: LoopsPackage)(f: Exp[T] => Exp[R]): String =
    prog.emitSourceScala(f, "Test")


  def emitSourceScala2[T1 : Manifest, T2: Manifest, R : Manifest](prog: LoopsPackage)(f: (Exp[T1], Exp[T2]) => Exp[R]): String =
    prog.emitSourceScala2(f, "Test")

  def emitSourceJava[T : Manifest, R : Manifest](prog: LoopsPackage)(f: Exp[T] => Exp[R]): String =
    prog.javaGen.emitSource(f, "Test")

  def emitSourceJava2[T1 : Manifest, T2: Manifest, R : Manifest](prog: LoopsPackage)(f: (Exp[T1], Exp[T2]) => Exp[R]): String =
    prog.javaGen.emitSource2(f, "Test")
}
