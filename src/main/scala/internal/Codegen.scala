package scala.virtualization.lms
package internal

// Codegen knows how to emit top definition (as a class),
// how to emit val, vars and recursive defs.
trait Codegen extends GenericCodegen with Config {

  import SEMANTICS._

  override def kernelFileExt = LANGUAGE

  override def toString = LANGUAGE

  def emitTopDef[A: Manifest](args: List[Sym[_]], body: Block[A], className: String, out: java.io.PrintWriter) = {
    val sA = remap(manifest[A])
    withStream(out) {
      stream.println()
      stream.println(PUBLIC + CLASS + className + " " + EXTENDS + FUNC_INTERFACE(args, sA) + SERIALIZABLE_CLASS + " {")
      stream.println(PUBLIC + BEFORE_FUNC(sA) +"apply(" + args.map(a => VALUE(quote(a), remap(a.tp))).mkString(", ") + ")" + AFTER_FUNC(sA) + "{")
      emitBlock(body)
      stream.println(RETURN + quote(getBlockResult(body)) + E)
      stream.println("}")
      stream.println("}")
    }
  }

  def relativePath(fileName: String): String = {
    val i = fileName.lastIndexOf('/')
    fileName.substring(i + 1)
  }

  def emitValDef(sym: Sym[Any], rhs: String): Unit = {
    stream.println(FINAL + GET_TYPE(sym) + quote(sym) + " = " + rhs + E)
  }

  def emitAssignment(lhs: String, rhs: String): Unit = {
    stream.println(lhs + " = " + rhs + E)
  }
}

trait NestedCodegen extends GenericNestedCodegen with Codegen {
  val IR: Blocks

  import IR._
  import SEMANTICS._

  // emit forward decls for recursive vals
  override def traverseStmsInBlock[A](stms: List[Stm]): Unit = {
    recursive foreach emitForwardDef
    super.traverseStmsInBlock(stms)
  }

  def emitForwardDef(sym: Sym[Any]): Unit = {
    stream.println("var " + quote(sym) + /*": " + remap(sym.tp) +*/ " = null.asInstanceOf[" + remap(sym.tp) + "]")
  }

  // special case for recursive vals
  override def emitValDef(sym: Sym[Any], rhs: String): Unit = {
    if (recursive contains sym)
      stream.println(quote(sym) + " = " + rhs) // we have a forward declaration above.
    else
      super.emitValDef(sym, rhs)
  }

}

trait FatCodegen extends GenericFatCodegen with Codegen {
  val IR: Blocks with FatExpressions
}
