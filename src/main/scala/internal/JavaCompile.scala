package scala.virtualization.lms.internal

import java.io.File
import java.net.{URI, URLClassLoader}
import javax.tools.JavaFileObject.Kind.SOURCE
import javax.tools.ToolProvider.getSystemJavaCompiler
import javax.tools._

import j.{JFunction1, JFunction2}

import scala.collection.JavaConverters._
import scala.virtualization.lms.internal.JavaCompile.nextClassName

trait JavaCompile extends Expressions {
  val javaGen: Codegen { val IR: JavaCompile.this.type }

  def compileJava[A,B](f: Exp[A] => Exp[B])(implicit mA: Manifest[A], mB: Manifest[B]): JFunction1[A, B] = {
    val name = nextClassName
    compile(javaGen.emitSource(f, name), name).newInstance().asInstanceOf[JFunction1[A,B]]
  }

  def compileJava2[A,B,C](f: (Exp[A], Exp[B]) => Exp[C])(implicit mA: Manifest[A], mB: Manifest[B], mC: Manifest[C]): JFunction2[A, B, C] = {
    val name = nextClassName
    compile(javaGen.emitSource2(f, name), name).newInstance().asInstanceOf[JFunction2[A,B,C]]
  }

  def compile (source: String, name: String): Class[_] = {
    val file = new SimpleJavaFileObject (URI.create("file:///" + name.replace('.','/') + SOURCE.extension), SOURCE){
      override def getCharContent(ignoreEncodingErrors: Boolean) = "import j.*;\nimport java.util.*;\n\n".concat(source)
    }
    val java_compiler = getSystemJavaCompiler
    java_compiler.getStandardFileManager(null, null, null)
    val files:List[JavaFileObject] = List(file)
    val dest = System.getProperty("java.io.tmpdir")
    val classpath = this.getClass.getClassLoader match {
      case ctx: java.net.URLClassLoader => ctx.getURLs.map(_.getPath).mkString(File.pathSeparator)
      case _ => System.getProperty("java.class.path")
    }

    val options = List("-classpath", classpath, "-d", dest)
    val task = java_compiler.getTask(null, null, null, options.asJava, null, files.asJava)
    if (!task.call()) println("compilation's terminated with errors")
    val class_url = new File(dest).toURI.toURL
    val urls = Array(class_url)
    val loader = new URLClassLoader(urls, this.getClass.getClassLoader)
    loader.loadClass(name)
  }
}

object JavaCompile {
  var n = 0
  def nextClassName = {
    n += 1
    "javaTempClass$" + n
  }
}
