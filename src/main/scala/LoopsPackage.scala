package scala.virtualization.lms

import common._
import scala.virtualization.lms.internal.{Codegen, Exp, JavaCompile, ScalaCompile}

trait LoopsAPI
  extends IfThenElse
  with Functions
  with PrimitiveOps
  with BooleanOps
  with NumericOps
  with LiftNumeric
  with ArrayLoops
  with ArrayLoopsOps
  with OrderingOps

trait BaseGen
  extends Codegen
  with GenBooleanOps
  with GenFatArrayLoopsFusionOpt
  with GenNumericOps
  with GenOrderingOps {
  val IR: LoopsPackage
}

trait LoopsPackage
  extends IfThenElseExp
  with FunctionsRecursiveExp
  with PrimitiveOpsExp
  with BooleanOpsExp
  with NumericOpsExp
  with IfThenElseFatExp
  with ArrayLoopsFatExp
  with OrderingOpsExp
  with ScalaCompile
  with JavaCompile {
  self =>
  var fuse = false

  val scalaGen = new BaseGen
    with GenFunctions
    with GenIfThenElseFat {
    val IR: self.type = self
    val SEMANTICS = new ScalaClasses
    override def shouldApplyFusion(currentScope: List[Stm])(result: List[Exp[Any]]): Boolean = fuse
  }

  val javaGen = new BaseGen
    with JavaGenFunctions
    with JavaGenIfThenElseFat {
    val IR: self.type = self
    val SEMANTICS = new JavaClasses
    override def shouldApplyFusion(currentScope: List[Stm])(result: List[Exp[Any]]): Boolean = fuse
  }
}
