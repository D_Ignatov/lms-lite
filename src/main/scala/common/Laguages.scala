package scala.virtualization.lms
package common

import scala.reflect.RefinedManifest
import scala.virtualization.lms.internal.Sym

trait Semantics {
  def PUBLIC: String
  def FINAL: String
  def VAR(javaType : String): String
  def FINAL(javaType : String): String
  def VALUE(name: String, aType: String): String
  def RETURN: String
  def E: String
  def CLASS = b("class")
  def EXTENDS: String
  def BEFORE_FUNC(sA: String): String
  def AFTER_FUNC(sA: String): String
  def LANGUAGE: String
  def WITH_GENERICS(tp: String): String
  def INT_LOOP(v: String, limit: String): String
  def GET_TYPE[T](sym: Sym[T]): String
  def b(s: String) = s + " "
}

trait LanguageClasses {
  def NEW_ARRAY(generics: String): String
  def ARRAY_GET: String
  def MUT_ARRAY(generics: String): String
  def ARRAY_SIZE: String
  def ARRAY_RESULT: String
  def ARRAY_ADD(v: String): String
  def ARRAY_ADD(v: String, id: String): String
  def SERIALIZABLE_CLASS: String
  def FUNC_INTERFACE(inTypes: List[Sym[_]], resultType: String): String
  def toClass(tp: String): String
  def remap[A](m: Manifest[A]): String = remapImp(m)
  def remapImp[A](m: Manifest[A]): String = m match {
    case rm: RefinedManifest[A] => "AnyRef{" + rm.fields.foldLeft("") { (acc, f) => {
      val (n, mnf) = f
      acc + "val " + n + ": " + remapImp(mnf)
    }
    } + "}"
    case _ =>
      // call remap on all type arguments
      val targs = m.typeArguments
      if (targs.length > 0) {
        val ms = m.toString
        ms.take(ms.indexOf("[") + 1) + targs.map(tp => remapImp(tp)).mkString(", ") + "]"
      }
      else m.toString
  }
}

trait JavaSemantics extends Semantics { self: LanguageClasses =>
  def PUBLIC = b("public")
  def FINAL: String = b("final")
  def FINAL(javaType : String) = b("final") + b(javaType)
  def VAR(javaType : String) = b(javaType)
  def VALUE(name: String, aType: String) = aType + " " + name
  def RETURN = b("return")
  def E = ";"
  def EXTENDS = b("implements")
  def BEFORE_FUNC(sA: String) = b(sA)
  def AFTER_FUNC(sA: String) = ""
  def LANGUAGE: String = "java"
  def GET_TYPE[T](sym: Sym[T]): String = b(remap(sym.tp))
  def INT_LOOP(v: String, limit: String) = s"for(int $v = 0; $v < $limit; $v++)"
  def WITH_GENERICS(tp: String): String = {
    val regex = """^(.*)\[([^\]]+)]$""".r
    tp match {
      case regex(name, generics) if counterpartClass.isDefinedAt(name) &&
        generics.split(",").forall(g => counterpartClass.isDefinedAt(g.replace(" ", "").toLowerCase)) =>
        toClass(name) + generics.split(",").map(g => toClass(g.replace(" ", ""))).mkString("<", ",", ">")
      case _ => tp
    }
  }
  val counterpartClass = Map("boolean" -> "Boolean", "int" -> "Integer", "long" -> "Long", "float" -> "Float",
    "double" -> "Double", "string" -> "String", "array" -> "ArrayList", "function1" -> "JFunction1")
}

trait ScalaSemantics extends Semantics { self: LanguageClasses =>
  def PUBLIC = ""
  def FINAL = b("val")
  def FINAL(javaType : String) = b("val")
  def VAR(javaType : String) = b("var")
  def VALUE(name: String, aType: String) = name + " : " + aType
  def E = ""
  def RETURN = ""
  def EXTENDS = b("extends")
  def BEFORE_FUNC(sA: String) = b("def")
  def AFTER_FUNC(sA: String) = " : " + sA + " = "
  def FUNC_INTERFACE(inTypes: List[Sym[_]], resultType: String) = "((" + inTypes.map(a => remap(a.tp)).mkString(", ") + ")=>(" + resultType + ")) "
  def LANGUAGE: String = "scala"
  def WITH_GENERICS(tp: String) = tp
  def INT_LOOP(v: String, limit: String) = s"for ($v <- 0 until $limit)"
  def GET_TYPE[T](sym: Sym[T]) = ""
}

class JavaClasses extends JavaSemantics with LanguageClasses {
  def NEW_ARRAY(generics: String) = s" new ArrayList<$generics>"
  def ARRAY_GET = ".get"
  def MUT_ARRAY(generics: String) = NEW_ARRAY(generics)
  def ARRAY_SIZE = ".size()"
  def ARRAY_RESULT = ""
  def ARRAY_ADD(v: String) = s".add($v)"
  def ARRAY_ADD(v: String, id: String) = ARRAY_ADD(v)
  def SERIALIZABLE_CLASS = ", java.io.Serializable"
  def FUNC_INTERFACE(inTypes: List[Sym[_]], resultType: String) = "JFunction" + inTypes.length + "<" + inTypes.map(a => remap(a.tp)).mkString(", ") + ", " + resultType + ">"
  override def remap[T](tp: Manifest[T]): String =
    if (remapImp(tp).startsWith("scala")) toClass(tp.runtimeClass.getSimpleName) else toClass(remapImp(tp))
  def toClass(tp: String) =
    counterpartClass.getOrElse(tp.toLowerCase, WITH_GENERICS(tp.toLowerCase))
}

class ScalaClasses extends ScalaSemantics with LanguageClasses {
  def ARRAY_GET = ""
  def NEW_ARRAY(generics: String) = s" new Array[$generics]"
  def MUT_ARRAY(generics: String) = s"scala.collection.mutable.ArrayBuilder.make[$generics]"
  def ARRAY_SIZE = ".length"
  def ARRAY_RESULT = ".result()"
  def ARRAY_ADD(v: String) = s" += $v "
  def ARRAY_ADD(v: String, id: String) = s"($id) = $v"
  def SERIALIZABLE_CLASS = " with Serializable"
  def toClass(tp: String) = tp
}
