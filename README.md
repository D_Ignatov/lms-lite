# LMS-lite

This repo is a very simplified version of [LMS](https://github.com/TiarkRompf/virtualization-lms-core).
It may be useful to take a look at [LMS website](http://scala-lms.github.io/).

The main purpose of this mini-project in pedagogical: to ease study LMS approach. This is work in progress.

## Verbose testing

By default, tests print no information. However, it is possible to make them verbose (to see generated Scala code):


    $ sbt
    > eval System.setProperty("test.verbose", "true")
    > test

## Task for a vacancy of Scala developer in Huawei Research Russia (Parallel Programming Models Lab)

Look at tests. Tests demonstrate facility of LMS to stage a program in mini-language, compile it to Scala and execute compiled code.
Tests check correctness of transformation: generated code should compile and produce expected value on run.

Your task is to extend this mini-framework with facility to generate Java code.
You should implement `scala.virtualization.lms.internal.JavaCompile`, mix this implementation into `LoopsPackage`
and extend tests with checks that your implementation is correct (borrow the style of existing tests).

Please fork this repo in private mode, do this task in your forked repo and invite me to your repo when you are done.

Good luck!
